const genericCrud = require("./generic.controller");
const { Client } = require('../models');

module.exports = {
  ...genericCrud(Client),
};