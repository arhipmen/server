// Importing required modules
const cors = require('cors');
const express = require('express');
const bodyParser = require('body-parser');
const http = require('http');
const { routes } = require('./routes/index')

// parse env variables
require('dotenv').config();
require("./helpers/db/mongodb.js")();

// Configuring port
const port = process.env.PORT || 9000;

const app = express();

// Configure middlewares
app.use(cors());
app.use(bodyParser.json());

app.set('view engine', 'html');

// Static folder
app.use(express.static(__dirname + '/views/'));

// Defining route middleware
routes.forEach(i =>{
  app.use(`/${i}`, require(`./routes/${i}`));
})

// Listening to port
http.createServer({}, app).listen(port)
console.log(`Server running http://localhost:${port}`);

module.exports = app;
