const router = require("express-promise-router")();

const {checkJWTSign} = require('../middlewares/jwtCheck.middleware')
const { setting } = require('../controllers');

router.route("/:id").get(setting.get);
router.route("/").post(setting.create);
router.route("/").get(setting.getAll);
router.route("/:id").put(setting.update);
router.route("/:id").delete(setting.delete);

module.exports = router;
