const { Schema, model } = require('mongoose');

const userSchema = new Schema(
  {
    name: {
      type: String,
      default: ""
    },
    price: {
      type: String,
      default: ""
    },
    category: {
      type: String,
      default: ""
    },
    edit: {
      type: Boolean,
      default: false
    }
  }
);

module.exports = model('Service', userSchema);
