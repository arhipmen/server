const {Schema, model} = require('mongoose');

const userSchema = new Schema(
  {
    name: {
      type: String,
      default: ""
    },
    car: {
      type: String,
      default: ""
    },
    phone: {
      type: String,
      default: ""
    },
    carNumber: {
      type: String,
      default: ""
    },
    detail: {
      type: Array,
      default: [
        {
          name: {
            type: String,
            default: ""
          },
          quantity: {
            type: Number,
            default: null
          },
          price: {
            type: Number,
            default: null
          },
        }
      ]
    },
    job: {
      type: Array,
      default: [
        {
          name: {
            type: String,
            default: ""
          },
          price: {
            type: Number,
            default: null
          },
        }
      ]
    },
    sumDetails: {
      type: Number,
      default: null
    },
    sumJobs: {
      type: Number,
      default: null
    },
    sumAll: {
      type: Number,
      default: null
    },
    dataJobs: {
      type: String,
      default: ""
    },
    master: {
      type: String,
      default: ""
    }
  },
  {timestamps: true},
);

module.exports = model('Client', userSchema);
