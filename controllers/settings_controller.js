const genericCrud = require("./generic.controller");
const { Setting } = require('../models');

module.exports = {
  ...genericCrud(Setting),
};