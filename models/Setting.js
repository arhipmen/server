const { Schema, model } = require('mongoose');

const userSchema = new Schema(
  {
    title: {
      type: String,
      default: ""
    },
    h1: {
      type: String,
      default: ""
    },
    description: {
      type: String,
      default: ""
    },
    address: {
      type: String,
      default: ""
    },
    phone: {
      type: String,
      default: ""
    },
    jobTime: {
      type: String,
      default: ""
    },
    plusJob: {
      type: String,
      default: ""
    }
  },
  { timestamps: true },
);

module.exports = model('Setting', userSchema);
