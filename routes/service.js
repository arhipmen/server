const router = require("express-promise-router")();

const {checkJWTSign} = require('../middlewares/jwtCheck.middleware')
const { service } = require('../controllers');

router.route("/:id").get(service.get);
router.route("/").post(service.create);
router.route("/").get(service.getAll);
router.route("/:id").put(service.update);
router.route("/:id").delete(service.delete);

module.exports = router;
