module.exports = {
  client: require('./client_controller'),
  service: require('./services_controller'),
  setting: require('./settings_controller'),
  auth: require('./auth_controller'),
};
