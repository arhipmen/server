const genericCrud = require("./generic.controller");
const { Service } = require('../models');

module.exports = {
  ...genericCrud(Service),
};