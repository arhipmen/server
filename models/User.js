const { Schema, model } = require('mongoose');

const userSchema = new Schema(
  {
    email: {
      type: String,
      default: "",
    },
    password: {
      type: String,
      default: ''
    }
  },
  { timestamps: true },
);

module.exports = model('User', userSchema);
