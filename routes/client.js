const router = require("express-promise-router")();

const {checkJWTSign} = require('../middlewares/jwtCheck.middleware')
const { client } = require('../controllers');

router.route("/:id").get(client.get);
router.route("/").post(client.create);
router.route("/").get(client.getAll);
router.route("/:id").put(client.update);
router.route("/:id").delete(client.delete);

module.exports = router;
